/**
 * Created by Тимофей on 27.04.2016.
 */

   google.load("visualization", "1", {packages:["corechart"]});
   google.setOnLoadCallback(drawChart);
   function drawChart() {
       var chartData = google.visualization.arrayToDataTable(data);
       var options = {
           title: 'Распределение учёных',
           is3D: true,
           pieResidueSliceLabel: 'Остальные'
       };
       var chart = new google.visualization.PieChart(document.getElementById('air'));
       chart.draw(chartData, options);
   }