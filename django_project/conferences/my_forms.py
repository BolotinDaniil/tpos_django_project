from conferences.models import Scientist, City, Theme, Conference, Participant
from django import forms
from datetime import date
from django.forms import widgets
from django.forms import widgets

FIELD_FOR_SORT = (
    ('finish_date','Дата окончания'),
    ('start_date','Дата начала'),
    ('city_id','Город'),
    ('theme_id', 'Научная тематика'),
    ('name', 'Имя'),
)


class DateSelectorWidget(widgets.MultiWidget):
    def __init__(self, attrs=None):
        # create choices for days, months, years
        # example below, the rest snipped for brevity.
        years = [(year, year) for year in range(1900,2020)]
        months = [(month, month) for month in range(1,13)]
        days = [(day,day) for day in range(1,32)]
        _widgets = (
            widgets.Select(attrs=attrs, choices=days),
            widgets.Select(attrs=attrs, choices=months),
            widgets.Select(attrs=attrs, choices=years),
        )
        super(DateSelectorWidget, self).__init__(_widgets, attrs)

    def decompress(self, value):
        if value:
            return [value.day, value.month, value.year]
        return [None, None, None]

    def format_output(self, rendered_widgets):
        return u''.join(rendered_widgets)

    def value_from_datadict(self, data, files, name):
        datelist = [
            widget.value_from_datadict(data, files, name + '_%s' % i)
            for i, widget in enumerate(self.widgets)]
        try:
            D = date(day=int(datelist[0]), month=int(datelist[1]),
                    year=int(datelist[2]))
        except ValueError:
            return ''
        else:
            return str(D)

class ScientistForm(forms.ModelForm):
    city = forms.ModelChoiceField(queryset = City.objects.all(), empty_label = None, label = 'Город')
    theme = forms.ModelChoiceField(queryset = Theme.objects.all(), empty_label = None, label = 'Научная тематика')

    class Meta:
        model = Scientist
        fields = ['last_name', 'first_name','middle_name', 'birthday', 'city', 'theme']
        labels = {
            'last_name':   ('Фамилия'),
            'first_name':  ('Имя'),
            'middle_name': ('Отчество'),
            'birthday':    ('Дата рождения'),
            'city':        ('Город'),
            'theme':       ('Научная тематика'),
        }
        widgets = {
            'birthday' : DateSelectorWidget(),
        }

class CityForm(forms.ModelForm):
    class Meta:
        model = City
        fields = ['name', 'population', 'category']
        labels = {
            'name': 'Имя',
            'population':'Количество жителей',
            'category':'Категория',
        }

class ThemeForm(forms.ModelForm):
    class Meta:
        model = Theme
        fields = ['name', 'type']
        labels = {
            'name':'Название тематики',
            'type':'Тип',
        }

class ConferenceForm(forms.ModelForm):
    city = forms.ModelChoiceField(queryset=City.objects.all(), empty_label=None, label='Город')
    theme = forms.ModelChoiceField(queryset=Theme.objects.all(), empty_label=None, label='Научная тематика')

    class Meta:
        model = Conference
        fields = ['name', 'city', 'theme', 'start_date', 'finish_date', 'level']
        widgets = {
            'start_date': DateSelectorWidget(),
            'finish_date': DateSelectorWidget(),
        }
        labels = {
            'name'       :'Название',
            'city'       :'Город',
            'theme'      :'Научная тематика',
            'start_date' :'Дата начала',
            'finish_date':'Дата окончания',
            'level'      :'Уровень',
        }

class ParticipantForm(forms.ModelForm):
    # scientist = forms.ModelChoiceField(queryset = Scientist.objects.all(),
    #                                    empty_label = None, label = 'Учёный')

    # чтобы были видны только учёные тематики конференции
    def __init__(self, *args, **kwargs):
        my_param = kwargs.pop('conf_theme', None)
        super(ParticipantForm, self).__init__(*args, **kwargs)

        if my_param != None:
            self.fields['scientist'] = forms.ModelChoiceField(queryset=Scientist.objects.all().filter(theme=my_param),
                                                      empty_label=None, label='Учёный')

    class Meta:
        model = Participant
        fields = ['scientist','cnt_reports']
        labels = {
            'scientist': ('Учёный'),
            'cnt_reports':('Количество докладов')
        }

class ConferenceReportForm(forms.Form):
    # start_date = forms.DateField(label = 'Дата начала')
    # finish_date = forms.DateField(label = 'Дата окончания')
    city = forms.ModelChoiceField(queryset = City.objects.all(),
                                  empty_label = 'Все города',
                                  label = 'Город')
    theme = forms.ModelChoiceField(queryset = Theme.objects.all(),
                                    empty_label = 'Все научные тематики',
                                    label = 'Научная тематика'
                                    )
    field_for_sort = forms.ChoiceField(choices = FIELD_FOR_SORT,
                                       label = 'Отсортировать по'
                                        )
    show_old = forms.BooleanField(label = 'Отображать прошедшие конференции',
                                  required = False, initial = True
                                  )

class NameForm(forms.Form):
    first_name = forms.CharField(label='Имя ', max_length=100)
    second_name = forms.CharField(label = 'Фамилия ', max_length = 100)

class loginForm(forms.Form):
    user_name = forms.CharField(label='Имя ', max_length=100)
    password = forms.CharField(label = 'Пароль ', max_length = 100, widget = forms.PasswordInput)

