from django.shortcuts import render,redirect
from conferences.models import Scientist, City, Theme, Conference, Participant
import conferences.reports as reports
import django.contrib.auth as djauth
from django.contrib.auth import authenticate
import datetime

from conferences.my_forms import *

def add_scientist(request):
    if not request.user.is_authenticated():
        return redirect('/login/')
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:

        scientist = ScientistForm(request.POST)
        # check whether it's valid:
        if scientist.is_valid():
            scientist.save()
            return thanks(request)
        else: return

    # if a GET (or any other method) we'll create a blank form
    else:
        scientist = ScientistForm()

    return render(request, 'conferences/modify_model.html', {'title':'Добавление учёного', 'form': scientist})

def add_city(request):
    if not request.user.is_authenticated():
        return redirect('/login/')
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        city = CityForm(request.POST)
        # check whether it's valid:
        if city.is_valid():
            city.save()
            return thanks(request)

    # if a GET (or any other method) we'll create a blank form
    else:
        city = CityForm()

    context =  {'title':'Добавление города', 'form': city}
    return render(request, 'conferences/modify_model.html',context)

def add_theme(request):
    if not request.user.is_authenticated():
        return redirect('/login/')
    if request.method == 'POST':
        theme = ThemeForm(request.POST)
        if theme.is_valid():
            theme.save()
            return thanks(request)

    # if a GET (or any other method) we'll create a blank form
    else:
        theme = ThemeForm()
    context = {'title':'Добавление тематики', 'form': theme}
    return render(request, 'conferences/modify_model.html', context)

def add_conference(request):
    if not request.user.is_authenticated():
        return redirect('/login/')
    if request.method == 'POST':
        conference_form = ConferenceForm(request.POST)
        if conference_form.is_valid():
            conference_form.save()
            return thanks(request)
        else:
            return
    # if a GET (or any other method) we'll create a blank form
    else:
        conference_form = ConferenceForm()

    context = {'title': 'Добавление конференции', 'form': conference_form}
    return render(request, 'conferences/modify_model.html', context)

def modify_scientist(request, s_id):
    if not request.user.is_authenticated():
        return redirect('/login/')
    scientist = Scientist.objects.get(id = s_id)
    if request.method == 'POST':
        scientist_form = ScientistForm(request.POST, instance = scientist)
        if scientist_form.is_valid():
            scientist_form.save()
            return thanks(request)
        else:
            return

    # if a GET (or any other method) we'll create a blank form
    else:
        scientist_form = ScientistForm(instance = scientist)

    context = {'title': 'Изменение параметров учёного', 'form': scientist_form}
    return render(request, 'conferences/modify_model.html', context)

def modify_city(request, c_id):
    if not request.user.is_authenticated():
        return redirect('/login/')
    city = City.objects.get(id = c_id)
    if request.method == 'POST':
        city_form = CityForm(request.POST, instance = city)
        if city_form.is_valid():
            city_form.save()
            return thanks(request)
        else:
            return

    # if a GET (or any other method) we'll create a blank form
    else:
        city_form = CityForm(instance = city)

    context = {'title': 'Изменение параметров города', 'form': city_form}
    return render(request, 'conferences/modify_model.html', context)


def modify_theme(request, t_id):
    if not request.user.is_authenticated():
        return redirect('/login/')
    theme = Theme.objects.get(id = t_id)
    if request.method == 'POST':
        theme_form = ThemeForm(request.POST, instance = theme)
        if theme_form.is_valid():
            theme_form.save()
            return thanks(request)
        else:
            return
    # if a GET (or any other method) we'll create a blank form
    else:
        theme_form = ThemeForm(instance = theme)

    context = {'title': 'Изменение параметров научной тематики', 'form': theme_form}
    return render(request, 'conferences/modify_model.html', context)

def modify_conference(request, c_id):
    if not request.user.is_authenticated():
        return redirect('/login/')
    conference = Conference.objects.get(id = c_id)
    if request.method == 'POST':
        conference_form = ConferenceForm(request.POST, instance = conference)
        if conference_form.is_valid():
            conference_form.save()
            return thanks(request)
        else:
            return
    # if a GET (or any other method) we'll create a blank form
    else:
        conference_form = ConferenceForm(instance = conference)

    context = {'title': 'Изменение параметров конференции', 'form': conference_form}
    return render(request, 'conferences/modify_model.html', context)

def delete_scientist(request, s_id):
    if not request.user.is_authenticated():
        return redirect('/login/')
    return_path = request.META.get('HTTP_REFERER', '/')
    Scientist.objects.get(id = s_id).delete()
    return redirect(return_path)


def delete_city(request, c_id):
    if not request.user.is_authenticated():
        return redirect('/login/')
    return_path = request.META.get('HTTP_REFERER', '/')
    City.objects.get(id=c_id).delete()
    return redirect(return_path)

def delete_theme(request, t_id):
    if not request.user.is_authenticated():
        return redirect('/login/')
    return_path = request.META.get('HTTP_REFERER', '/')
    Theme.objects.get(id=t_id).delete()
    return redirect(return_path)

def delete_conference(request, c_id):
    if not request.user.is_authenticated():
        return redirect('/login/')
    return_path = request.META.get('HTTP_REFERER', '/')
    Conference.objects.get(id=c_id).delete()
    return redirect(return_path)

def delete_participant(request, c_id,p_id):
    if not request.user.is_authenticated():
        return redirect('/login/')
    return_path = request.META.get('HTTP_REFERER', '/')
    Participant.objects.all().filter(conference_id = c_id, scientist_id = p_id).delete()
    return redirect(return_path)



def scientists(request):
    s = Scientist.objects.all()
    sc = list(s)
    # print(sc[0].city)
    return render(request, 'conferences/scientists.html', {'scientists':sc})

def cities(request):
    c = City.objects.all()
    cs = list(c)
    data = []
    for csi in cs:
        cnt = len(list(Scientist.objects.filter(city = csi)))
        data.append((csi.name, cnt))
    return render(request, 'conferences/cities.html', {'cities':cs, 'diagram_data':data})

def themes(request):
    t = Theme.objects.all()
    ts = list(t)
    data = []
    for tsi in ts:
        cnt = len(list(Scientist.objects.filter(theme=tsi)))
        data.append((tsi.name, cnt))
    return render(request, 'conferences/themes.html', {'themes':ts, 'diagram_data':data})

def scientist_information(request, s_id):
    if request.method == "POST":
        if 'get_scientist_confs' in request.POST:
            path = reports.build_report_scientist_confs(s_id)
        elif 'get_scientist_theme_confs' in request.POST:
            path = reports.build_report_scientist_theme_confs(s_id)
        elif 'get_scientist_city_confs' in request.POST:
            path = reports.build_report_scientist_city_confs(s_id)
        elif 'get_scientist_theme_confs_cities' in request.POST:
            path = reports.build_report_scientist_theme_confs_cities(s_id)
        return redirect(path)

    scn = Scientist.objects.get(id = s_id)
    prts = Participant.objects.all().filter(scientist = s_id)
    confs_scientist = [prtsi.conference for prtsi in prts]
    confs_theme = Conference.objects.all().filter(theme = scn.theme)
    confs_city = Conference.objects.all().filter(city = scn.city)
    cities_scientist = City.objects.raw('SELECT * FROM City WHERE id IN '
                                        '(SELECT city_id FROM Conference WHERE Theme_id = %s)',
                                        [scn.theme_id])
    context = {
        's':scn,
        'confs_scientist':confs_scientist,
        'confs_theme' : confs_theme,
        'confs_city' : confs_city,
        'cities_scientist' : cities_scientist,
    }
    return render(request, 'conferences/scientist_information.html', context)

def scientist_conferences_report(request, s_id):
    path = reports.build_report_scientist_confs(s_id)
    return redirect(path)


def conferences(request):
    form_data = {}

    if request.method == "POST":
        # берём сырые данные из формы, если они вообще переданы
        city_id = request.POST.get('city')
        theme_id = request.POST.get('theme')
        show_old = request.POST.get('show_old', False)
        field_for_sort = request.POST.get('field_for_sort')
        form_data['city'] = city_id
        form_data['theme'] = theme_id
        form_data['show_old'] = show_old
        form_data['field_for_sort'] = field_for_sort

        if city_id == '':
            city_id = None
        else : city_id = int(city_id)
        if theme_id == '':
            theme_id = None
        else:
            theme_id = int(theme_id)

        if 'get_xls' in request.POST:
            path = reports.build_report_conferences(city_id, theme_id, show_old, field_for_sort)
            return redirect(path)
        else:
            # view new data
            cl = Conference.get_by_theme_and_city(city_id, theme_id, show_old, field_for_sort)
    else :

        c = Conference.objects.all()
        cl = list(c)
    report_form = ConferenceReportForm(initial=form_data)
    context = {
        'conferences': cl,
        'form':report_form
    }
    return render(request, 'conferences/conferences.html', context)


def participants(request, c_id):
    if request.method == "POST":
        pf = ParticipantForm(request.POST)
        if pf.is_valid():
            p = pf.save(commit=False)
            old_conf = Conference.objects.get(id = c_id).id
            old_scn = p.scientist.id
            Participant.objects.all().filter(scientist = old_scn, conference = old_conf).delete()
            p.conference = Conference.objects.get(id = c_id)
            p.save()

    conf_theme = Conference.objects.get(id = c_id).theme
    pf = ParticipantForm(conf_theme = conf_theme)
    p = Participant.objects.all().filter(conference_id__exact = c_id)
    pl = list(p)
    context = {
        'participants' : pl,
        'form' : pf
    }
    return render(request, 'conferences/participants.html', context)


def login(request):
    if request.user.is_authenticated():
        return redirect('/')
    return_path = request.META.get('HTTP_REFERER','/')
    print("PATH = ",return_path)
    # если мы пришли с этой страницы
    p = return_path.find(request.get_full_path())
    if (p != -1 ):
        return_path = return_path[:p+1]
    print(request.get_full_path())
    if request.method == "POST":
        signin = loginForm(request.POST)
        if signin.is_valid():
            user_name = signin.cleaned_data['user_name']
            password  = signin.cleaned_data['password']
            user = authenticate(username=user_name, password=password)
            if user != None and user.is_active:
                djauth.login(request, user)
                return redirect(return_path)
        else:
            print('invalid data!!')
    else:
        signin = loginForm()
    context = {'form' : signin}
    return render(request, 'conferences/login.html', context)

def logout(request):
    return_path = request.META.get('HTTP_REFERER','/')
    djauth.logout(request)
    return redirect(return_path)

def thanks(request):
    return render(request, 'conferences/thanks.html')

def main(request):
    return render(request, 'conferences/main.html')