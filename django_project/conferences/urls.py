from django.conf.urls import url
from django.contrib import admin

from django.conf.urls.static import static
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
import conferences.views

urlpatterns = [
    url(r'^scientists/', conferences.views.scientists, name = 'scientists'),
    url(r'^scientist/(?P<s_id>\d{1,})/$', conferences.views.scientist_information, name = 'scientist_information'),
    url(r'^cities/', conferences.views.cities, name = 'cities'),
    url(r'^themes/', conferences.views.themes, name = 'themes'),
    url(r'^conferences/', conferences.views.conferences, name = 'conferences'),
    url(r'^conference/(?P<c_id>\d{1,})/participants', conferences.views.participants, name = 'participants'),

    url(r'^add_s',conferences.views.add_scientist, name = 'add_scientist'),
    url(r'^add_city/',conferences.views.add_city, name = 'add_city'),
    url(r'^add_theme/',conferences.views.add_theme, name = 'add_theme'),
    url(r'^add_conference/',conferences.views.add_conference, name = 'add_conference'),
    url(r'^thanks/', conferences.views.thanks, name  = 'thanks'),

    url(r'^modify_scientist/(?P<s_id>\d{1,})/', conferences.views.modify_scientist, name='modify_scientist'),
    url(r'^modify_city/(?P<c_id>\d{1,})/', conferences.views.modify_city, name='modify_city'),
    url(r'^modify_theme/(?P<t_id>\d{1,})/', conferences.views.modify_theme, name='modify_theme'),
    url(r'^modify_conference/(?P<c_id>\d{1,})/', conferences.views.modify_conference, name='modify_conference'),

    url(r'^delete_scientist/(?P<s_id>\d{1,})/', conferences.views.delete_scientist, name='delete_scientist'),
    url(r'^delete_city/(?P<c_id>\d{1,})/', conferences.views.delete_theme, name='delete_theme'),
    url(r'^delete_conference/(?P<c_id>\d{1,})/', conferences.views.delete_conference, name='delete_conference'),
    url(r'^conference/(?P<c_id>\d{1,})/delete_participant/(?P<p_id>\d{1,})/', conferences.views.delete_participant, name='delete_participant'),

    url(r'^scientist/(?P<s_id>\d{1,})/conferences/', conferences.views.scientist_conferences_report, name='scientist_conferences_report'),

    url(r'^login/', conferences.views.login, name = 'login'),
    url(r'^logout/', conferences.views.logout, name = 'logout'),
    url(r'^$', conferences.views.main, name = 'main'),
]


urlpatterns += static(settings.REPORT_URL, document_root=settings.REPORT_ROOT)
# urlpatterns += staticfiles_urlpatterns()