from django.test import TestCase
import unittest
from conferences.models import Scientist, City, Theme, Conference, Participant
from datetime import date


class CityTestCase(TestCase):
    def setUp(self):
        City.objects.create(name='Город100500', population=100500, category='Федеральный')

    def testCityStrMethod(self):
        city = City.objects.get(name='Город100500')
        self.assertEqual(str(city), 'Город100500', 'City.__str__() is not correct')

    def testExceptionOnWrongCategory(self):
        self.assertRaises(AssertionError, City.create, name='badCity', population=10, category='1')

    def testExceptionOnWrongPopulation(self):
        self.assertRaises(AssertionError, City.create, name='badCity', population=-1, category='Федеральный')


class ScientistTestCase(TestCase):
    def setUp(self):
        city = City.objects.create(name='Город100500', population=100500, category='Федеральный')
        theme = Theme.objects.create(name='Тематика100500', type='технический')
        Scientist.objects.create(first_name='Иван', last_name='Иванов', middle_name='Иванович',
                                 city=city, theme=theme, birthday=date(1980, 5, 7))

    def testScientistStrMethod(self):
        scientist = Scientist.objects.get(first_name='Иван')
        self.assertEqual(str(scientist), 'Иванов', 'Scientist.__str__() is not corrent')

    def testScientistFullNameMethod(self):
        scientist = Scientist.objects.get(first_name='Иван')
        self.assertEqual(scientist.get_full_name(), 'Иванов Иван Иванович', 'Scientist.get_full_name() is not corrent')