import xlrd, xlwt
from conferences.models import Scientist, City, Theme, Conference, Participant
from django.db import connection

def build_report_conferences(city_id, theme_id, show_old, field_for_sort):
    '''
    :return: url path to report
    '''
    header = (
        'Имя',
        'Тематика',
        'Город',
        'Уровень',
        'Дата начала',
        'Дата окончания',
        'Количество докладов',
        'Доля докладов из других городов (в %)',
    )
    confs = Conference.get_by_theme_and_city(city_id, theme_id, show_old, field_for_sort)
    data = []
    cursor = connection.cursor()
    for ci in confs:
        a = []
        a.append(ci.name)
        a.append(ci.theme.name)
        a.append(ci.city.name)
        a.append(ci.level)
        a.append(str(ci.start_date))
        a.append(str(ci.finish_date))
        cursor.execute('SELECT SUM(Cnt_Reports) FROM Participant WHERE Conference_id = %s', [ci.id])
        row = cursor.fetchone()
        if row[0] == None:
            a.append(0)
        else : a.append(row[0])


        cursor.execute('SELECT 100.0*b.s/a.s FROM (SELECT SUM(Cnt_Reports) as s FROM Participant WHERE Conference_id = %s) AS a'
                       ', (SELECT SUM(Cnt_Reports) as s FROM Participant '
                            'WHERE Conference_id = %s and Scientist_id IN'
                                '(SELECT id FROM Scientist WHERE City_id <> %s)'
                       ') AS b'
                       ,
                       [ci.id, ci.id, ci.city.id])

        row = cursor.fetchone()
        if row[0] == None:
            a.append(0)
        else:
            a.append(round(row[0],2))

        data.append(a)
    path = get_report_from_list(header, data, 'История конференций', 'confs')
    return path

def build_report_scientist_confs(s_id):
    '''
        :return: url path to report
    '''
    header = (
        'Имя',
        'Тематика',
        'Город',
        'Уровень',
        'Дата начала',
        'Дата окончания',
    )
    s = Scientist.objects.get(id=s_id)

    prts = Participant.objects.raw('SELECT * FROM Participant WHERE Scientist_ID = %s', [s_id])
    data = []
    for ci in [prtsi.conference for prtsi in prts]:
        a = []
        a.append(ci.name)
        a.append(ci.theme.name)
        a.append(ci.city.name)
        a.append(ci.level)
        a.append(str(ci.start_date))
        a.append(str(ci.finish_date))
        data.append(a)
    path = get_report_from_list(header, data, 'Конференции учёного', 'scientist_confs')
    return path

def build_report_scientist_theme_confs(s_id):
    header = (
        'Имя',
        'Тематика',
        'Город',
        'Уровень',
        'Дата начала',
        'Дата окончания',
    )
    s = Scientist.objects.get(id = s_id)
    confs = Conference.objects.raw('SELECT * FROM Conference WHERE theme_id = %s', [s.theme.id])
    data = []
    for ci in confs:
        a = []
        a.append(ci.name)
        a.append(ci.theme.name)
        a.append(ci.city.name)
        a.append(ci.level)
        a.append(str(ci.start_date))
        a.append(str(ci.finish_date))
        data.append(a)
    path = get_report_from_list(header, data, 'Конференции по тематике учёного', 'confs')
    return path


def build_report_scientist_city_confs(s_id):
    header = (
        'Имя',
        'Тематика',
        'Город',
        'Уровень',
        'Дата начала',
        'Дата окончания',
    )
    s = Scientist.objects.get(id=s_id)
    confs = Conference.objects.raw('SELECT * FROM Conference WHERE city_id = %s', [s.city.id])
    data = []
    for ci in confs:
        a = []
        a.append(ci.name)
        a.append(ci.theme.name)
        a.append(ci.city.name)
        a.append(ci.level)
        a.append(str(ci.start_date))
        a.append(str(ci.finish_date))
        data.append(a)
    path = get_report_from_list(header, data, 'Конференции в городе учёного', 'confs')
    return path

def build_report_scientist_theme_confs_cities(s_id):
    header = (
        'Имя',
        'Численность',
        'Категория',
    )
    s = Scientist.objects.get(id=s_id)
    # confs_theme = Conference.objects.all().filter(theme=s.theme)
    # cities_scientist = [conf.city for conf in confs_theme]
    cities_scientist = City.objects.raw('SELECT * FROM City WHERE id IN '
                                        '(SELECT city_id FROM Conference WHERE Theme_id = %s)',
                                        [s.theme_id])
    data = []
    for ci in cities_scientist:
        a = []
        a.append(ci.name)
        a.append(ci.population)
        a.append(ci.category)
        data.append(a)
    path = get_report_from_list(header, data, 'Города', 'cities')
    return path


def get_report_from_list(header, data, sheet_name = '', file_name = 'report'):
    '''
    :param header: list or touple of colums names string
    :param data: touple of touple with data
    :param sheet_name:
    :param file_name: name of output file, will add '.xls'
    :return:
    '''
    file_name += '.xls'
    wb = xlwt.Workbook()
    ws = wb.add_sheet(sheet_name)
    j = 0
    for hi in header:
        ws.write(0, j, hi)
        j += 1

    i = 0
    for di in data:
        i += 1
        j = 0
        for dij in di:
            ws.write(i, j, dij)
            j += 1

    wb.save('conferences/docs/' + file_name)
    return ('/docs/' + file_name)
