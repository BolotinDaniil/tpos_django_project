from django.db import models
from datetime import date

LENGTH_VARCHAR = 100

CITY_CATEGORIES = [
    ('Федеральный','Федеральный'),
    ('Региональный','Региональный'),
    ('Районный','Районный'),
]

THEME_TYPES = [
    ('гумманитарный', 'гумманитарный'),
    ('технический', 'технический'),
]

CONFERENCE_LEVEL = [
    ('Международная', 'Международная'),
    ('Федеральная', 'Федеральная'),
    ('Региональная', 'Региональная'),
    ('Районная', 'Районная'),
]



class City(models.Model):
    name = models.CharField(max_length=LENGTH_VARCHAR)
    population = models.IntegerField()
    category = models.CharField(max_length=LENGTH_VARCHAR,
                                choices=CITY_CATEGORIES,
                                default=CITY_CATEGORIES[0][0])

    @classmethod
    def create(cls, name, population, category):
        assert population >= 0

        ok = False
        for _, city_category in CITY_CATEGORIES:
            if category == city_category:
                ok = True
                break
        assert ok

        return cls.objects.create(name=name, population=population, category=category)

    class Meta:
        db_table = 'city'
    def __str__(self):
        return self.name

class Theme(models.Model):
    name = models.CharField(max_length=LENGTH_VARCHAR)
    type = models.CharField(max_length=LENGTH_VARCHAR, choices=THEME_TYPES,
                            default=THEME_TYPES[0][1])

    class Meta:
        db_table = 'theme'

    def __str__(self):
        return self.name

class Scientist(models.Model):
    first_name = models.CharField(max_length=LENGTH_VARCHAR)
    last_name = models.CharField(max_length=LENGTH_VARCHAR)
    middle_name = models.CharField(max_length=LENGTH_VARCHAR)
    city = models.ForeignKey(City, null=False)
    theme = models.ForeignKey(Theme, null=False)
    birthday = models.DateField(default=date(1900, 1, 1))

    class Meta:
        db_table = 'scientist'

    def __str__(self):
        return self.last_name

    def get_full_name(self):
        return self.last_name + ' ' + self.first_name + ' ' + self.middle_name

class Conference(models.Model):
    name = models.CharField(max_length=LENGTH_VARCHAR)
    city = models.ForeignKey(City, null=False)
    theme = models.ForeignKey(Theme, null=False)
    start_date = models.DateField(default=date(1900,1,1))
    finish_date = models.DateField(default=date(1900,1,1))
    level = models.CharField(max_length=LENGTH_VARCHAR, choices=CONFERENCE_LEVEL,
                             default=CONFERENCE_LEVEL[3][1])
    participants = models.ManyToManyField(Scientist, through = 'Participant')

    class Meta:
        db_table = 'conference'

    @staticmethod
    def get_by_theme_and_city(city_id, theme_id, show_old, field_for_sort):
        query = ''
        params = []
        if city_id == None and theme_id == None:
            query = 'SELECT * FROM conference'
        elif city_id == None:
            query = 'SELECT * FROM conference where theme_id = %s'
            params = [theme_id]
        elif theme_id == None:
            query = 'SELECT * FROM conference where city_id = %s'
            params = [city_id]
        else:
            query = 'SELECT * FROM conference where city_id = %s and theme_id = %s'
            params = [city_id, theme_id]
        if not show_old:
            td = date.today()
            params.append(td)
            if len(params) > 1:
                query += ' and finish_date > %s'
            else:
                query += ' where finish_date > %s'
        print(field_for_sort)
        query += ' order by ' + field_for_sort
        conf = Conference.objects.raw(query, params)
        return conf

    def __str__(self):
        return self.name

class Participant(models.Model):
    scientist = models.ForeignKey(Scientist)
    conference = models.ForeignKey(Conference)
    cnt_reports = models.IntegerField()

    class Meta:
        db_table = 'participant'
        unique_together = ("scientist", "conference")

    def __str__(self):
        return self.name



