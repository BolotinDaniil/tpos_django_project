ROOT_PACKAGE_NAME = 'django_project'

# requirements = list(map(lambda x: x.replace('\n', ''), open("requirements.txt").readlines()))

from setuptools import setup, find_packages

setup(
    name=ROOT_PACKAGE_NAME,
    version='0.1',
    author=['Bolotin Daniil'],
    author_email=['bolotin.di@phystech.edu'],
    package_data={'': ['*.sqlite3', '*.xls', '*.html', '*.py', '*.js']},
    packages=['djproj', 'conferences', 'proj_runner'],
    include_package_data=True,
    # install_requires=requirements,
    install_requires=['django==1.10.4', 'xlrd==1.2.0', 'xlwt==1.3.0'],
    description='Example package',
    # long_description=read_description(),
    license='GNU LGPL',
    keywords=['TPOS'],
    zip_safe=False,
    entry_points={'console_scripts': ['django_project = proj_runner.starter:main']},
    platforms=["Linux"],
    classifiers=[
        'Intended Audience :: Developers',
        'Programming Language :: Python'
    ]
)


