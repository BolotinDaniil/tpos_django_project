#!/bin/bash
yum -y update
yum -y install https://centos7.iuscommunity.org/ius-release.rpm
yum -y install rpm-build rpm-devel rpmlint make bash coreutils diffutils rpmdevtools
yum -y install python36u python36u-pip
pip3.6 install --upgrade pip
curl 'https://gitlab.com/BolotinDaniil/tpos_django_project/-/jobs/artifacts/master/raw/django_project/dist/django_project-0.1-1.noarch.rpm?job=deploy_centos&private_token=syoCsBdyY7CyRx7ez7Fm' --location --output django_project-0.1-1.noarch.rpm
yum install -y django_project-0.1-1.noarch.rpm
django_project migrate
django_project runserver

