#!/bin/bash
sudo apt update
sudo apt -q -y install python3 python3-pip
pip3 install --upgrade pip
curl 'https://gitlab.com/BolotinDaniil/tpos_django_project/-/jobs/artifacts/master/raw/django_project/dist/django_project-0.1-py3-none-any.whl?job=deploy_ubuntu&private_token=syoCsBdyY7CyRx7ez7Fm' --location --output django_project-0.1-py3-none-any.whl
pip3 install django_project-0.1-py3-none-any.whl
django_project migrate
django_project runserver

